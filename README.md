2. Given an input list of numbers, find the largest and the smallest numbers, print any numbers that are palindromes, and any numbers that are prime numbers.
Example: Given a list of [234, 678, 131, 101, 199, 346], the output should be:
Largest number: 678
Smallest number: 101
Palindromes: 131, 101
Prime numbers: 101, 131, 199