package ro.pentastagiu.main;

import ro.pentastagiu.listOfNumbers.ListOfNumbers;

public class Main {

	public static void main(String[] args) {
		
		int givenList[]={234,678,131,101,199,346};
		ListOfNumbers list1=new ListOfNumbers(givenList);
		list1.printResults();
		
		ListOfNumbers list2=new ListOfNumbers();
		list2.readList();
		list2.printResults();	
	}
}
