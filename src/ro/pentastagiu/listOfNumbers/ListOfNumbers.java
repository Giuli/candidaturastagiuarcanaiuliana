package ro.pentastagiu.listOfNumbers;
import java.util.*;

public class ListOfNumbers {
	private int length;
	private int []list;
	
	public ListOfNumbers(){}//default constructor
	
	public ListOfNumbers(int length){
		this.length=length;
		this.list=new int[this.length];
	}
	
	public ListOfNumbers(int []y){
		//Constuctor
		this.length=y.length;
		this.list=new int[this.length];
		for(int i=0;i<this.length;i++)
			this.list[i]=y[i];
	}
	
	public int getLength(){
		return this.length;
	}
	
	public void setLength(int n){
		this.length=n;
	}
	
	public int[] getlist(){
		return this.list;
	}
	
	public void setlist(int v[]){
		this.length=v.length;
		this.list=new int[this.length];
		for(int i=0;i<this.length;i++)
			list[i]=v[i];
	}
	
	public void read(){
		System.out.print("Insert the list elements: ");
		Scanner input=new Scanner(System.in);
		for(int i=0;i<this.length;i++)
			this.list[i]=input.nextInt();
		input.close();
	}
	
	public void readList(){
		Scanner input=new Scanner(System.in);
		System.out.print("Insert the list dimmension: ");
		this.length=input.nextInt();
		this.list=new int[this.length];
		System.out.println("Insert the list elements: ");
		for(int i=0;i<this.length;i++){
			System.out.print("list["+i+"]=");
			this.list[i]=input.nextInt();
		}
		input.close();
	}
	
	public void print(){
		System.out.print("List of numbers: ");
		for(int i=0;i<this.length-1;i++)
			System.out.print(this.list[i]+", ");
		System.out.print(this.list[length-1]+"\n");
	}
	
	public int largestNumber(){
		int i;
		int max;
		max=this.list[0];
		for(i=1;i<this.length;i++){
			if(this.list[i]>max)
				max=list[i];
		}
		return max;
	}
	
	public int smallestNumber(){
		int i;
		int min;
		min=this.list[0];
		for(i=1;i<this.length;i++){
			if(this.list[i]<min)
				min=list[i];
		}
		return min;
	}
	
	public boolean palindrom(int n){
		int r,sum=0,temp;
		boolean isPalindrom=false;
		temp=n;
		while(n>0){
			r=n%10;
			sum=(sum*10)+r; 
			n=n/10;
		}
		if(temp==sum) 
			isPalindrom=true ;
		return isPalindrom;
	}
	
	public void palindromInlist(){
		int list[]=new int [this.length];
		int nr=0;
		boolean ok;
		System.out.print("Palindroms: ");
		for(int i=0;i<this.length;i++){
			ok=palindrom(this.list[i]);
			if(ok) {
				list[nr]=this.list[i];
				nr++;
			}
		}
		if(nr==0) System.out.print("there are no palindroms in the given list!");
		else{
			int palindromList[]=new int[nr];
			for(int i=0;i<nr;i++)
				palindromList[i]=list[i];
			for(int i=0;i<nr-1;i++)
				System.out.print(palindromList[i]+", ");
			System.out.print(palindromList[nr-1]);
		}
	}
	
	public boolean primeNumber(int n){
		int temp;
		boolean isPrime=true;
		for(int i=2;i<=n/2;i++){
			temp=n%i;
			if(temp==0){
				isPrime=false;
				break;
			}
		}
		return isPrime;
	}
	
	public void primeInlist(){
		int nr=0;
		boolean ok;
		int list[]=new int [this.length];
		System.out.print("\nPrime numbers: ");
		for(int i=0;i<this.length;i++){
			ok=primeNumber(this.list[i]);
			if(ok) {
				list[nr]=this.list[i];
				nr++;
			}
		}
		if(nr==0) System.out.print("there are no prime numbers in the given list!\n");
		else{
			int primeList[]=new int[nr];
			for(int i=0;i<nr;i++)
				primeList[i]=list[i];
			for(int i=0;i<nr-1;i++)
				System.out.print(primeList[i]+", ");
			System.out.print(primeList[nr-1]);
		}
		System.out.println("\n");
	}
	
	public void printResults(){
		this.print();
		System.out.println("Largest number: "+this.largestNumber());
		System.out.println("Smallest number: "+this.smallestNumber());
		this.palindromInlist();
		this.primeInlist();
	}
}
